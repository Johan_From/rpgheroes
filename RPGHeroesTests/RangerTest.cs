﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.Heroes;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    public class RangerTest
    {
        [Fact]
        public static void CreateRanger_CreatedARangerAndCheckStartingLevel_ShouldReturnValid()
        {
            // Arrange 
            Ranger testRanger = new("Johan From");
            int expected = 1;

            // Act
            int actual = testRanger.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRanger_CreatedAnRangerAndLevelUp_ShouldReturnValid()
        {
            // Arrange 
            Ranger testRanger = new("Johan From");
            testRanger.LevelUp();
            int expected = 2;

            // Act
            int actual = testRanger.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRanger_CreatedAnRangerAndCheckStartingStartingAttributes_ShouldReturnValid()
        {
            // Arrange 
            Ranger testRanger = new("Johan From");
            int expected = 9;

            // Act
            int actual = testRanger.BaseStatistics.Strength + testRanger.BaseStatistics.Intelligence + testRanger.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRanger_CreatedAnRangerAndCheckStartingLevelUpAttributes_ShouldReturnValid()
        {
            // Arrange 
            Ranger testRanger = new("Johan From");
            testRanger.LevelUp();
            int expected = 16;

            // Act
            int actual = testRanger.BaseStatistics.Strength + testRanger.BaseStatistics.Intelligence + testRanger.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRanger_CreateAnRangerrWithHighLevelArmor_ShouldThrowLevelToLowException()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Armor testArmor = new("Quiver", 2, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<LevelToLowException>(() => testRanger.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateRanger_CreateAnRangerrWithArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Armor testArmor = new("Robe", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(1, 1, 1));

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testRanger.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateRanger_CreateRangerWithAxe_ShouldThrowInvalidWeapon()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Weapon testWeapon = new("Axe", 1, 1, 1, Weapons.AXE);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testRanger.EquipWeapon(testWeapon));

        }

        [Fact]
        public static void CreateRanger_CreateRangerWithWeapon_ShouldReturnValidMessage()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 1, 1, Weapons.BOW);
            string expected = "New weapon equipped!";

            // Act
            string actual = testRanger.EquipWeapon(testWeapon);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateRanger_CreateRangerWithArmor_ShouldReturnValidMessage()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Armor testArmor = new("Quiver", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(1, 1, 1));
            string expected = "New armour equipped!";

            // Act
            string actual = testRanger.EquipArmor(testArmor);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateRanger_CalculateDamageIfNoWeaponEquipped_ExpectDamageEqualsOne()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            double expected = 1 * (1 + (5 / 100));
            // Act
            double actual = testRanger.CalculateDamage(testRanger);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRanger_CalculateDamageWithValidWeapon_ExpectDamageMoreThanOne()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 7, 1.1, Weapons.BOW);
            testRanger.EquipWeapon(testWeapon);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testRanger.CalculateDamage(testRanger);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRanger_CalculateDamageWithValidWeaponAndValidArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Ranger testRanger = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 7, 1.1, Weapons.BOW);
            Armor testArmor = new("Quiver", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(1, 1, 1));
            testRanger.EquipWeapon(testWeapon);
            testRanger.EquipArmor(testArmor);
            testRanger.CalculateTotalAttributes(testRanger);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testRanger.CalculateDamage(testRanger);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRanger_CalculateAttributesWithWeaponAndArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Ranger testRanger = new("Johan From"); 
            Weapon testWeapon = new("Bow", 1, 7, 1.1, Weapons.BOW);
            Armor testArmor = new("Quiver", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(1, 1, 1));
            testRanger.EquipWeapon(testWeapon);
            testRanger.EquipArmor(testArmor);
            testRanger.CalculateTotalAttributes(testRanger);

            double expected = 8;

            // Act
            double actual = testRanger.TotalStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
