﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.Heroes;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    public class RogueTest
    {
        [Fact]
        public static void CreateRogue_CreatedARogueAndCheckStartingLevel_ShouldReturnValid()
        {
            // Arrange 
            Rogue testRogue = new("Johan From");
            int expected = 1;

            // Act
            int actual = testRogue.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRogue_CreatedAnRogueAndLevelUp_ShouldReturnValid()
        {
            // Arrange 
            Rogue testRogue = new("Johan From");
            testRogue.LevelUp();
            int expected = 2;

            // Act
            int actual = testRogue.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRogue_CreatedAnRogueAndCheckStartingStartingAttributes_ShouldReturnValid()
        {
            // Arrange 
            Rogue testRogue = new("Johan From");
            int expected = 9;

            // Act
            int actual = testRogue.BaseStatistics.Strength + testRogue.BaseStatistics.Intelligence + testRogue.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRogue_CreatedAnRogueAndCheckStartingLevelUpAttributes_ShouldReturnValid()
        {
            // Arrange 
            Rogue testRogue = new("Johan From");
            testRogue.LevelUp();
            int expected = 15;

            // Act
            int actual = testRogue.BaseStatistics.Strength + testRogue.BaseStatistics.Intelligence + testRogue.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateRogue_CreateAnRogueWithHighLevelArmor_ShouldThrowLevelToLowException()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Armor testArmor = new("Blade Mail", 2, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<LevelToLowException>(() => testRogue.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateRogue_CreateAnRoguerWithArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Armor testArmor = new("Blade Mail", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testRogue.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateRogue_CreateAnRoguerWithBow_ShouldThrowInvalidWeapon()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 1, 1, Weapons.BOW);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testRogue.EquipWeapon(testWeapon));

        }

        [Fact]
        public static void CreateRogue_CreateRogueWithWeapon_ShouldReturnValidMessage()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Weapon testWeapon = new("Sword", 1, 1, 1, Weapons.SWORD);
            string expected = "New weapon equipped!";

            // Act
            string actual = testRogue.EquipWeapon(testWeapon);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateRogue_CreateRogueWithArmor_ShouldReturnValidMessage()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Armor testArmor = new("Blade Mail", 1, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(2, 2, 2));
            string expected = "New armour equipped!";

            // Act
            string actual = testRogue.EquipArmor(testArmor);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateRogue_CalculateDamageIfNoWeaponEquipped_ExpectDamageEqualsOne()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            double expected = 1 * (1 + (5 / 100));
            // Act
            double actual = testRogue.CalculateDamage(testRogue);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRogue_CalculateDamageWithValidWeapon_ExpectDamageMoreThanOne()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Weapon testWeapon = new("Dagger", 1, 7, 1.1, Weapons.DAGGER);
            testRogue.EquipWeapon(testWeapon);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testRogue.CalculateDamage(testRogue);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRogue_CalculateDamageWithValidWeaponAndValidArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Rogue testRogue = new("Johan From");
            Weapon testWeapon = new("Dagger", 1, 7, 1.1, Weapons.DAGGER);
            Armor testArmor = new("Blade mail", 1, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(1, 1, 1));
            testRogue.EquipWeapon(testWeapon);
            testRogue.EquipArmor(testArmor);
            testRogue.CalculateTotalAttributes(testRogue);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testRogue.CalculateDamage(testRogue);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateRogue_CalculateAttributesWithWeaponAndArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Rogue testRogue = new("Johan From"); 
            Weapon testWeapon = new("Dagger", 1, 7, 1.1, Weapons.DAGGER);
            Armor testArmor = new("Blade mail", 1, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(1, 1, 1));
            testRogue.EquipWeapon(testWeapon);
            testRogue.EquipArmor(testArmor);
            testRogue.CalculateTotalAttributes(testRogue);

            double expected = 7;

            // Act
            double actual = testRogue.TotalStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
