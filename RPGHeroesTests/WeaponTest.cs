﻿using RPGHeroes.Enums;
using RPGHeroes.HeroItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    /// <summary>
    /// Armor test that follows the AAA standard, Arrange, Act and Assert
    /// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
    /// </summary>
    public class WeaponTest
    {
        [Fact]
        public void CreateWeapon__CreateAnWeapon_ShouldReturnValidString()
        {
            // Arrange
            Weapon weaponToBeTested = new Weapon("Iron Sword", 2, 10, 3, Weapons.SWORD);
            string expected = "Iron Sword";

            // Assert
            string actual = weaponToBeTested.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WeaponEquals_CreateAndTestIfEqual_ShouldReturnTrue()
        {
            // Arrange
            Weapon weaponToBeTestedOne = new("Axe", 2, 15, 1, Weapons.AXE);
            Weapon weaponToBeTestedTwo = new("Axe", 2, 15, 1, Weapons.AXE);
            bool expected = true;

            // Act
            bool actual = weaponToBeTestedOne.Equals(weaponToBeTestedTwo);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WeaponEquals_CreateAndTestIfEqual_ShouldReturnFalse()
        {
            // Arrange
            Weapon weaponToBeTestedOne = new("Axe", 2, 15, 1, Weapons.AXE);
            Weapon weaponToBeTestedTwo = new("Sword", 2, 15, 1, Weapons.SWORD);
            bool expected = false;

            // Act
            bool actual = weaponToBeTestedOne.Equals(weaponToBeTestedTwo);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
