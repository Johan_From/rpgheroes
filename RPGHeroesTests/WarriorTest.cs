﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.Heroes;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    /// <summary>
    /// Armor test that follows the AAA standard, Arrange, Act and Assert
    /// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
    /// </summary>
    public class WarriorTest
    {
        [Fact]
        public static void CreateWarrior_CreatedAnWarriorAndCheckStartingLevel_ShouldReturnValid()
        {
            // Arrange 
            Warrior testWarrior = new("Johan From");
            int expected = 1;

            // Act
            int actual = testWarrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateWarrior_CreatedAnWarriorAndLevelUp_ShouldReturnValid()
        {
            // Arrange 
            Warrior testWarrior = new("Johan From");
            testWarrior.LevelUp();
            int expected = 2;

            // Act
            int actual = testWarrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateWarrior_CreatedAnWarriorAndCheckStartingStartingAttributes_ShouldReturnValid()
        {
            // Arrange 
            Warrior testWarrior = new("Johan From");
            int expected = 8;

            // Act
            int actual = testWarrior.BaseStatistics.Strength + testWarrior.BaseStatistics.Intelligence + testWarrior.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateWarrior_CreatedAnWarriorAndCheckStartingLevelUpAttributes_ShouldReturnValid()
        {
            // Arrange 
            Warrior testWarrior = new("Johan From");
            testWarrior.LevelUp();
            int expected = 14;

            // Act
            int actual = testWarrior.BaseStatistics.Strength + testWarrior.BaseStatistics.Intelligence + testWarrior.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateWarrior_CreateAnWarriorWithHighLevelArmor_ShouldThrowLevelToLowException()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Armor testArmor = new("Blade Mail", 2, Slot.BODY, Material.PLATE, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<LevelToLowException>(() => testWarrior.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateWarrior_CreateAnWarriorWithArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Armor testArmor = new("Blade Mail", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateWarrior_CreateAnWarriorWithBow_ShouldThrowInvalidWeapon()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 1, 1, Weapons.BOW);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testWeapon));

        }

        [Fact]
        public static void CreateWarrior_CreateWarriorWithWeapon_ShouldReturnValidMessage()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Weapon testWeapon = new("Sword", 1, 1, 1, Weapons.SWORD);
            string expected = "New weapon equipped!";

            // Act
            string actual = testWarrior.EquipWeapon(testWeapon);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateWarrior_CreateWarriorWithArmor_ShouldReturnValidMessage()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Armor testArmor = new("Blade Mail", 1, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(2, 2, 2));
            string expected = "New armour equipped!";

            // Act
            string actual = testWarrior.EquipArmor(testArmor);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateWarrior_CalculateDamageIfNoWeaponEquipped_ExpectDamageEqualsOne()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            double expected = 1 * (1 + (5 / 100));
            // Act
            double actual = testWarrior.CalculateDamage(testWarrior);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateWarrior_CalculateDamageWithValidWeapon_ExpectDamageMoreThanOne()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Weapon testWeapon = new("Axe", 1, 7, 1.1, Weapons.AXE);
            testWarrior.EquipWeapon(testWeapon);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testWarrior.CalculateDamage(testWarrior);

            // Assert
            Assert.Equal(expected, actual);
        
        }

        [Fact]
        public static void CreateWarrior_CalculateDamageWithValidWeaponAndValidArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Warrior testWarrior = new("Johan From");
            Weapon testWeapon = new("Axe", 1, 7, 1.1, Weapons.AXE);
            Armor testArmor = new("Plate armor", 1, Slot.BODY, Material.PLATE, AttributeCreator.CreateAttributes(1, 1, 1));
            testWarrior.EquipWeapon(testWeapon);
            testWarrior.EquipArmor(testArmor);
            testWarrior.CalculateTotalAttributes(testWarrior);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testWarrior.CalculateDamage(testWarrior);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateWarrior_CalculateAttributesWithWeaponAndArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Warrior testWarrior = new("Johan From"); // Warrior has 5 Strength
            Weapon testWeapon = new("Axe", 1, 7, 1.1, Weapons.AXE);
            Armor testArmor = new("Plate armor", 1, Slot.BODY, Material.PLATE, AttributeCreator.CreateAttributes(1, 1, 1));
            testWarrior.EquipWeapon(testWeapon);
            testWarrior.EquipArmor(testArmor);
            testWarrior.CalculateTotalAttributes(testWarrior); // Calculate with 5 Strength + Armor Strength

            double expected = 6; 

            // Act
            double actual = testWarrior.TotalStatistics.Strength;

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
