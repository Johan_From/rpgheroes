﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.Heroes;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    /// <summary>
    /// Armor test that follows the AAA standard, Arrange, Act and Assert
    /// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
    /// </summary>
    public class MageTest
    {
        [Fact]
        public static void CreateMage_CreatedAnMageAndCheckStartingLevel_ShouldReturnValid()
        {
            // Arrange 
            Mage testMage = new("Johan From");
            int expected = 1;

            // Act
            int actual = testMage.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateMage_CreatedAnMageAndLevelUp_ShouldReturnValid()
        {
            // Arrange 
            Mage testMage = new("Johan From");
            testMage.LevelUp();
            int expected = 2;

            // Act
            int actual = testMage.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateMage_CreatedAnMageAndCheckStartingStartingAttributes_ShouldReturnValid()
        {
            // Arrange 
            Mage testMage = new("Johan From");
            int expected = 10;

            // Act
            int actual = testMage.BaseStatistics.Strength + testMage.BaseStatistics.Intelligence + testMage.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public static void CreateMage_CreatedAnMageAndCheckStartingLevelUpAttributes_ShouldReturnValid()
        {
            // Arrange 
            Mage testMage = new("Johan From");
            testMage.LevelUp();
            int expected = 17;

            // Act
            int actual = testMage.BaseStatistics.Strength + testMage.BaseStatistics.Intelligence + testMage.BaseStatistics.Dexterity;

            // Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public static void CreateMage_CreateAnMageWithHighLevelArmor_ShouldThrowLevelToLowException()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Armor testArmor = new("Robe", 2, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<LevelToLowException>(() => testMage.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateMage_CreateAnMageWithArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Armor testArmor = new("Not Robe", 1, Slot.BODY, Material.PLATE, AttributeCreator.CreateAttributes(2, 2, 2));

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => testMage.EquipArmor(testArmor));

        }

        [Fact]
        public static void CreateMage_CreateAnMageWithBow_ShouldThrowInvalidWeapon()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Weapon testWeapon = new("Bow", 1, 1, 1, Weapons.BOW);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => testMage.EquipWeapon(testWeapon));

        }

        [Fact]
        public static void CreateMage_CreateMageWithWeapon_ShouldReturnValidMessage()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Weapon testWeapon = new("Staff", 1, 1, 1, Weapons.BIG_STICK);
            string expected = "New weapon equipped!";

            // Act
            string actual = testMage.EquipWeapon(testWeapon);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateMage_CreateMageWithArmor_ShouldReturnValidMessage()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Armor testArmor = new("Robe", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(1, 1, 1));
            string expected = "New armour equipped!";

            // Act
            string actual = testMage.EquipArmor(testArmor);

            // Assert
            Assert.Equal(expected, actual);


        }

        [Fact]
        public static void CreateMage_CalculateDamageIfNoWeaponEquipped_ExpectDamageEqualsOne()
        {
            // Arrange
            Mage testMage = new ("Johan From");
            double expected = 1 * (1 + (5 / 100));
            // Act
            double actual = testMage.CalculateDamage(testMage);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateMage_CalculateDamageWithValidWeapon_ExpectDamageMoreThanOne()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Weapon testWeapon = new("Staff", 1, 7, 1.1, Weapons.BIG_STICK);
            testMage.EquipWeapon(testWeapon);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testMage.CalculateDamage(testMage);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateMage_CalculateDamageWithValidWeaponAndValidArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Mage testMage = new("Johan From");
            Weapon testWeapon = new("Staff", 1, 7, 1.1, Weapons.BIG_STICK);
            Armor testArmor = new("Plate armor", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(1, 1, 1));
            testMage.EquipWeapon(testWeapon);
            testMage.EquipArmor(testArmor);
            testMage.CalculateTotalAttributes(testMage);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testMage.CalculateDamage(testMage);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public static void CreateMage_CalculateAttributesWithWeaponAndArmor_ExpectDamageMoreThanOne()
        {
            // Arrange
            Mage testMage = new("Johan From"); 
            Weapon testWeapon = new("Staff", 1, 7, 1.1, Weapons.BIG_STICK);
            Armor testArmor = new("Robe", 1, Slot.BODY, Material.CLOTH, AttributeCreator.CreateAttributes(1, 1, 1));
            testMage.EquipWeapon(testWeapon);
            testMage.EquipArmor(testArmor);
            testMage.CalculateTotalAttributes(testMage);

            double expected = 9; // Intelligence 8 from beggining than add 1 for armor

            // Act
            double actual = testMage.TotalStatistics.Intelligence;

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}

