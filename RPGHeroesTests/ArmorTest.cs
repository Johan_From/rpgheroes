using RPGHeroes.Enums;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;

namespace RPGHeroesTests
{
    /// <summary>
    /// Armor test that follows the AAA standard, Arrange, Act and Assert
    /// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
    /// </summary>
    public class ArmorTest
    {
        [Fact]
        public void CreateArmor_CreatedArmor_ShouldReturnValidString()
        {
            // Arrange
            Armor armorToBeTested = new("Blademail", 2, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(0, 1, 2));
            string expected = "Blademail";

            // Assert
            string actual = armorToBeTested.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArmorEquals_CreateAndTestIfEqual_ShouldReturnTrue()
        {
            // Arrange
            Armor armorToBeTestedOne = new("Tuniqe", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(0, 1, 1));
            Armor armorToBeTestedTwo = new("Tuniqe", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(0, 1, 1));
            bool expected = true;

            // Act
            bool actual = armorToBeTestedOne.Equals(armorToBeTestedTwo);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArmorEquals_CreateAndTestIfEqual_ShouldReturnfalse()
        {
            // Arrange
            Armor armorToBeTestedOne = new("Tuniqe", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(0, 1, 1));
            Armor armorToBeTestedTwo = new("LetherCap", 1, Slot.BODY, Material.LEATHER, AttributeCreator.CreateAttributes(0, 1, 1));
            bool expected = false;

            // Act
            bool actual = armorToBeTestedOne.Equals(armorToBeTestedTwo);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}