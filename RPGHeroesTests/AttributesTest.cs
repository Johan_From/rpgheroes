﻿using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroesTests
{
    /// <summary>
    /// Armor test that follows the AAA standard, Arrange, Act and Assert
    /// MethodYouAreTesting_ConditionsItsBeingTestedUnder_ExpectedBehaviour().
    /// </summary>
    public class AttributesTest
    {
        [Fact]
        public void WarriorStartingAttributes_CreateWarriorStartingAttributesCheckStrength_ShouldReturnValidStartingValue()
        {
            // Arrange
            AttributesPowers warriorAttributes = AttributeCreator.WarriorStartingAttributes();
            int expected = 5;

            // Act
            int actual = warriorAttributes.Strength;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AttributesAddOperator_AddAttributesToWarriorStartingAttributes_ShouldReturnIncreasedAttributesWithTrue()
        {
            // Arrange
            bool expected = true;
            AttributesPowers warriorAttribues = AttributeCreator.WarriorStartingAttributes();
            AttributesPowers attributesToAdd = AttributeCreator.CreateAttributes(3, 3, 3);
            AttributesPowers totalAttributes = AttributeCreator.CreateAttributes(8, 5, 4);
            //AttributesPowers warriorAttributes = AttributeCreator.WarriorStartingAttributes();

            // Act
            bool actual = totalAttributes.Equals(warriorAttribues + attributesToAdd);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IAttributesEquals_AddAttributesToWarriorStartingAttributesAndTestEquals_ShouldReturnIncreasedAttributesWithFalse()
        {
            // Arrange
            AttributesPowers warriorAttribues = AttributeCreator.WarriorStartingAttributes();
            AttributesPowers attributesToAdd = AttributeCreator.CreateAttributes(1, 1, 1);
            AttributesPowers totalAttributes = AttributeCreator.CreateAttributes(3, 7, 2);

            bool expected = false;
            // Act
            bool actual = totalAttributes.Equals(warriorAttribues + attributesToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

    }
}
