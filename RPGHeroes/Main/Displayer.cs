﻿using RPGHeroes.Enums;
using RPGHeroes.Heroes;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Main
{
    public class Displayer
    {
        /// <summary>
        /// Method for handling menu change
        /// </summary>
        /// <returns>Boolean that is either true or false</returns>
        public static bool MainMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Show Armours");
            Console.WriteLine("2) Show Weapons");
            Console.WriteLine("3) Show Heroes");
            Console.WriteLine("0) Quit Program");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "0":
                    Console.WriteLine("Goodbye!\n");
                    return false;
                case "1":
                    DisplayAmours();
                    return true;
                case "2":
                    DisplayWeapons();
                    return true;
                case "3":
                    DisplayHeroes();
                    return true;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Method for displaying some weapons for controlling that all classes work
        /// </summary>
        public static void DisplayWeapons()
        {
            Console.Clear();

            Console.WriteLine("");

            Weapon weapon1 = new("Iron Sword", 1, 1, 1, Weapons.SWORD);
            Weapon weapon2 = new("Axe", 2, 3, 1, Weapons.AXE);

            StringBuilder sb = new();
            sb.AppendLine($"Weapon name: {weapon1.Name}");
            sb.AppendLine($"Required level: { weapon1.RequiredLevel}");
            sb.AppendLine($"Damage: {weapon1.Damage}");
            sb.AppendLine($"Attack speed: { weapon1.AttackSpeed}");
            sb.AppendLine($"Weapon type: {weapon1.Type}");
            sb.AppendLine($"Damage per second: {weapon1.DamagePerSecond}");
            sb.AppendLine("");
            sb.AppendLine($"Weapon name: {weapon2.Name}");
            sb.AppendLine($"Required level: {weapon2.RequiredLevel}");
            sb.AppendLine($"Damage: {weapon2.Damage}");
            sb.AppendLine($"Attack speed: {weapon2.AttackSpeed}");
            sb.AppendLine($"Weapon type: {weapon2.Type}");
            sb.AppendLine($"Damage per second: {weapon2.DamagePerSecond}");

            Console.WriteLine(sb);

        }

        /// <summary>
        /// Method for displaying some armours for checking that all classes work
        /// </summary>
        public static void DisplayAmours()
        {
            Console.Clear();
            StringBuilder sb = new();

            Armor armor1 = new("Blade Mail", 1, Slot.BODY, Material.MAIL, AttributeCreator.CreateAttributes(3, 2, 2));
            Armor armor2 = new("Shield ", 1, Slot.BODY, Material.PLATE, AttributeCreator.CreateAttributes(5, 1, 1));

            sb.AppendLine($"Armor name: {armor1.Name}");
            sb.AppendLine($"Required level: {armor1.RequiredLevel}");
            sb.AppendLine($"Required slot: {armor1.RequiredSlot}");
            sb.AppendLine($"Armour material: {armor1.Material}");
            sb.AppendLine($"Armour dexterity: {armor1.Attributes.Dexterity}");
            sb.AppendLine($"Armour strength: {armor1.Attributes.Strength}");
            sb.AppendLine($"Armour intelligence: {armor1.Attributes.Intelligence}");
            sb.AppendLine("");
            sb.AppendLine($"Armor name: {armor2.Name}");
            sb.AppendLine($"Required level: {armor2.RequiredLevel}");
            sb.AppendLine($"Required slot: {armor2.RequiredSlot}");
            sb.AppendLine($"Armour material: {armor2.Material}");
            sb.AppendLine($"Armour dexterity: {armor2.Attributes.Dexterity}");
            sb.AppendLine($"Armour strength: {armor2.Attributes.Strength}");
            sb.AppendLine($"Armour intelligence: {armor2.Attributes.Intelligence}");

            Console.WriteLine(sb);

        }

        /// <summary>
        /// Method for displaying the heroes of each class and check if they work
        /// </summary>
        public static void DisplayHeroes()
        {
            Console.Clear();

            StringBuilder sb = new();

            // Warriors damage will alter because of new weapon
            Warrior warrior = new("Ursa");
            warrior.LevelUp();
            Weapon weapon = new("Axe", 2, 2, 5, Weapons.AXE);
            warrior.EquipWeapon(weapon);
            warrior.CalculateTotalAttributes(warrior);

            // Mage damage wont alter because of no weapon
            Mage mage = new("Crystal Maiden");
            mage.LevelUp();

            // Rogue damage wont alter because of no weapon
            Rogue rogue = new("Slark");
            rogue.LevelUp();

            // Ranger damage will alter because of new weapon
            Ranger ranger = new("Windbreaker");
            ranger.LevelUp();
            ranger.LevelUp();
            Weapon weapon2 = new("Bow", 1, 2, 3, Weapons.BOW);
            ranger.EquipWeapon(weapon2);

            // Warrior
            sb.AppendLine($"Warrior name: {warrior.Name}");
            sb.AppendLine($"Warrior level: {warrior.Level}");
            sb.AppendLine($"Warrior strength: {warrior.TotalStatistics?.Strength}");
            sb.AppendLine($"Warrior intelligence: {warrior.TotalStatistics?.Intelligence}");
            sb.AppendLine($"Warrior dexterity: {warrior.TotalStatistics?.Dexterity}");
            sb.AppendLine($"Warrior damage: {warrior.CalculateDamage(warrior)}");
            
            foreach (Item item in warrior.Inventory.Values)
            {
                sb.AppendLine($"Item in inventory: {item.Name}");
            }

            // Mage
            sb.AppendLine("");
            sb.AppendLine($"Mage name: {mage.Name}");
            sb.AppendLine($"Mage level: {mage.Level}");
            sb.AppendLine($"Mage strength: {mage.BaseStatistics.Strength}");
            sb.AppendLine($"Mage intelligence: {mage.BaseStatistics.Intelligence}");
            sb.AppendLine($"Mage dexterity: {mage.BaseStatistics.Dexterity}");
            sb.AppendLine($"Mage damage: {mage.CalculateDamage(mage)}");
            
            // Rogue
            sb.AppendLine("");
            sb.AppendLine($"Rogue name: {rogue.Name}");
            sb.AppendLine($"Rogue level: {rogue.Level}");
            sb.AppendLine($"Rogue strength: {rogue.BaseStatistics.Strength}");
            sb.AppendLine($"Rogue intelligence: {rogue.BaseStatistics.Intelligence}");
            sb.AppendLine($"Rogue dexterity: {rogue.BaseStatistics.Dexterity}");
            sb.AppendLine($"Rogue damage: {rogue.CalculateDamage(rogue)}");

            // Ranger
            sb.AppendLine("");
            sb.AppendLine($"Ranger name: {ranger.Name}");
            sb.AppendLine($"Ranger level: {ranger.Level}");
            sb.AppendLine($"Ranger strength: {ranger.BaseStatistics.Strength}");
            sb.AppendLine($"Ranger intelligence: {ranger.BaseStatistics.Intelligence}");
            sb.AppendLine($"Ranger dexterity: {ranger.BaseStatistics.Dexterity}");
            sb.AppendLine($"Ranger damage: {ranger.CalculateDamage(ranger)}");
            foreach (Item item in mage.Inventory.Values)
            {
                sb.AppendLine($"Item in inventory: {item.Name}");
            }

            Console.WriteLine(sb);

        }
    }
}
