﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Enums
{
    /// <summary>
    /// Enum for slots
    /// </summary>
    public enum Slot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
