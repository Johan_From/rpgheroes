﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Enums
{
    /// <summary>
    /// Enum for weapons
    /// </summary>
    public enum Weapons
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        BIG_STICK,
        SWORD,
        SMALL_STICK
    }
}
