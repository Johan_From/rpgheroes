﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Enums
{
    /// <summary>
    /// Enum for materials
    /// </summary>
    public enum Material
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
