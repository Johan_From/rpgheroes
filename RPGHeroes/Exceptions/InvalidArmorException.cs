﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exceptions
{
    /// <summary>
    /// Exception class for invalid armor
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string? message) : base(message){}

        public override string Message => "Your character can't use this armor";
    }
}
