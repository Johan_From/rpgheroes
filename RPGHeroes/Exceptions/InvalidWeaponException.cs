﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exceptions
{
    /// <summary>
    /// Exception class for invalid weapon
    /// </summary>
    public class InvalidWeaponException : Exception
    {

        public InvalidWeaponException(string? message) : base(message){}

        public override string Message => "Your character can't use this weapon";

    }
}
