﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exceptions
{
    public class AttributeException : Exception
    {
        public AttributeException(string? message) : base(message){}

        public override string Message => "Could not calucalte attributes";
    }
}
