﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Exceptions
{
    /// <summary>
    /// Exception class for level to low
    /// </summary>
    public class LevelToLowException : Exception
    {

        public LevelToLowException(string? message) : base(message){}

        public override string Message => "Your character is too low of a level";
    }
}
