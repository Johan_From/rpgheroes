﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public abstract class Hero : IHero
    {
        #region Variables
        private string? _name;
        private int _level = 1;
        private double _dmg = 1;
        private Material[]? _accArmors;
        private Weapons[]? _accWeapons;
        private AttributesPowers? _baseStatistics;
        private AttributesPowers? _totalStatistics;
        private Dictionary<Slot, Item> _inventory = new();

        #endregion

        #region Attributes
        public string? Name { get { return _name; } set { _name = value; } }
        public int Level { get { return _level; } set { _level = value; } }
        public double Dmg { get { return _dmg; } set { _dmg = value; } }
        public Material[]? AccArmors { get { return _accArmors; } set { _accArmors = value; } }
        public Weapons[]? AccWeapons { get { return _accWeapons; } set { _accWeapons = value; } }

        public AttributesPowers? BaseStatistics { get { return _baseStatistics; } set { _baseStatistics = value; } }
        public AttributesPowers? TotalStatistics { get { return _totalStatistics; } set { _totalStatistics = value; } }

        public Dictionary<Slot, Item> Inventory { get { return _inventory; } set { _inventory = value; } }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for hero
        /// </summary>
        /// <param name="name"></param>
        /// <param name="baseStatistics"></param>
        /// <param name="accArmors"></param>
        /// <param name="accWeapons"></param>
        public Hero(string name, AttributesPowers baseStatistics, Material[] accArmors, Weapons[] accWeapons)
        {
            Name = name;
            BaseStatistics = baseStatistics;
            AccArmors = accArmors;
            AccWeapons = accWeapons;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Abstract void for leveling upp, will be specific for each hero
        /// </summary>
        public abstract void LevelUp();        

        /// <summary>
        /// Method for equiping an item
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="LevelToLowException"></exception>
        public void EquipItem(Item item)
        {
            if (Level >= item.GetLevel())
            {
                if (Inventory.ContainsKey(item.GetSlot()) == true) Inventory[item.GetSlot()] = item;

                if (Inventory.ContainsKey(item.GetSlot()) == false) Inventory.Add(item.GetSlot(), item);

            }
            else
            {
                // Custom Exception for level to low
                throw new LevelToLowException("Hero's level is to low!");
            }

        }

        /// <summary>
        /// String method for equping weapon
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Success message</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public string EquipWeapon(Weapon item)
        {
            if (AccWeapons!.Contains(item.GetWeaponType()))
            {
                EquipItem(item);
                return "New weapon equipped!";
            }
            else
            {
                // Custom exception for invalid weapon
                throw new InvalidWeaponException("Can't Equip the weapon");
            }
        }

        /// <summary>
        /// String method for equping armor
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Success message</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public string EquipArmor(Armor item)
        {      
            if (AccArmors!.Contains(item.GetArmorMaterial()))
            {
                EquipItem(item);
                return "New armour equipped!";
            }
            else
            {
                // Custom exception for invalid armour
                throw new InvalidArmorException("Can't Equip the armour");
            }
        }
        #endregion

    }
}
