﻿using RPGHeroes.HeroItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    /// <summary>
    /// BLueprint for Hero class
    /// </summary>
    internal interface IHero
    {
        public void LevelUp();
        public void EquipItem(Item item);
        public string EquipWeapon(Weapon item);
        public string EquipArmor(Armor item);
    }
}
