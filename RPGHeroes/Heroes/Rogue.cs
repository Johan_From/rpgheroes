﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Rogue : Hero
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        public Rogue(string name) : base(name, AttributeCreator.RogueStartingAttributes(), new Material[] { Material.LEATHER, Material.MAIL },
            new Weapons[] { Weapons.DAGGER, Weapons.SWORD }) {} 

        #endregion

        #region Methods

        /// <summary>
        /// Method for leveling upp
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += AttributeCreator.RogueLevelUp();
        }

        /// <summary>
        /// Method for calculating total attributes
        /// </summary>
        /// <param name="rogue"></param>
        /// <exception cref="Exception"></exception>
        public void CalculateTotalAttributes(Rogue rogue)
        {
            TotalStatistics = BaseStatistics; // totalStatistics is basestatistics which means total != null
            foreach (var item in rogue.Inventory) // Loop through the Dictionary
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor)) // If item value is Armor then do the calculation
                    {
                        Armor? armor = item.Value as Armor; // armor is the values
                        TotalStatistics += armor!.Attributes; // Add the attributs from armor to total statistics
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Could not caluclate attributes");
                }
            }
        }

        /// <summary>
        /// Method for calculating damage 
        /// </summary>
        /// <param name="rogue"></param>
        /// <returns>Return the damage as a double</returns>
        public double CalculateDamage(Rogue rogue)
        {
                try
                {
                    // If Dictionary not contains the key of enum WEAPON
                    if (rogue.Inventory.ContainsKey(Slot.WEAPON) == false) return Dmg;
                    
                    if(rogue.Inventory.ContainsKey(Slot.WEAPON) != false)
                    {
                        Weapon weapon = (Weapon)Inventory[Slot.WEAPON]; // Get the weapon in inventory
                        double damage = (double)(weapon.DamagePerSecond * (1 + BaseStatistics.Dexterity / 100)); // Damage formula
                        return damage;
                    }

                    return Dmg;
                }
                
                catch (InvalidWeaponException)
                {
                    // If exception throw error
                    throw new InvalidWeaponException("Could not caluclate the damage with the weapon!");
                }
            }
        #endregion
    }
}
