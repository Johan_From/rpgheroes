﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Warrior : Hero
    {
        #region Constructor
        /// <summary>
        /// Constructor for warrior
        /// </summary>
        /// <param name="name"></param>
        public Warrior(string name) : base(name, AttributeCreator.WarriorStartingAttributes(), new Material[] { Material.PLATE, Material.MAIL },
            new Weapons[] { Weapons.AXE, Weapons.HAMMER, Weapons.BIG_STICK, Weapons.SWORD }) {} 
        #endregion

        #region Methods
        /// <summary>
        /// Method for levelin up
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += AttributeCreator.WarriorLevelUp();
        }

        /// <summary>
        /// Method for calculating total damage
        /// </summary>
        /// <param name="warrior"></param>
        /// <exception cref="InvalidArmorException"></exception>
        public void CalculateTotalAttributes(Warrior warrior)
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in warrior.Inventory)
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor))
                    {
                        Armor? armor = item.Value as Armor;
                        TotalStatistics += armor!.Attributes;
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Could not caluclate total attributes with armor");
                }
            }
        }

        /// <summary>
        /// Method for calculating the damage
        /// </summary>
        /// <param name="warrior"></param>
        /// <returns>Return the damage as a double</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public double CalculateDamage(Warrior warrior)
        {
            try
            {
                if (warrior.Inventory.ContainsKey(Slot.WEAPON) == false) return Dmg;
                
                if(warrior.Inventory.ContainsKey(Slot.WEAPON) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.WEAPON];
                    double damage = (double)(weapon.DamagePerSecond * (1 + BaseStatistics.Strength / 100));
                    return damage;
                }

                return Dmg;
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Could not caluclate the damage with the weapon!");
            }
        }

        #endregion
    }
}
