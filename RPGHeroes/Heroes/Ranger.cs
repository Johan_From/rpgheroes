﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    
    public class Ranger :  Hero
    {
        #region Constructor
        /// <summary>
        /// Constructor for Ranger
        /// </summary>
        /// <param name="name"></param>
        public Ranger(string name) : base(name, AttributeCreator.RangerStartingAttributes(), new Material[] { Material.LEATHER, Material.MAIL },
            new Weapons[] { Weapons.BOW }) {} 
        #endregion

        #region Methods
        /// <summary>
        /// Method for leveling up
        /// </summary>
        public override void LevelUp()
        {
            Level += 1; // Add new level
            BaseStatistics += AttributeCreator.RangerLevelUp(); // Ranger statistics level up
        }

        /// <summary>
        /// Method for calculating total attributes
        /// </summary>
        /// <param name="ranger"></param>
        /// <exception cref="Exception"></exception>
        public void CalculateTotalAttributes(Ranger ranger)
        {
            TotalStatistics = BaseStatistics; // totalStatistics is basestatistics which means total != null
            foreach (var item in ranger.Inventory) // Loop through the Dictionary
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor)) // If item value is Armor then do the calculation
                    {
                        Armor? armor = item.Value as Armor; // armor is the values
                        TotalStatistics += armor!.Attributes; // Add the attributs from armor to total statistics
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Could not caluclate attributes");
                }
            }
        }

        /// <summary>
        /// Method for calculating damage 
        /// </summary>
        /// <param name="ranger"></param>
        /// <returns>Return the damage as a double</returns>
        public double CalculateDamage(Ranger ranger)
        {
            try
            {
                // If Dictionary not contains the key of enum WEAPON
                if (ranger.Inventory.ContainsKey(Slot.WEAPON) == false) return Dmg;
                
                if(ranger.Inventory.ContainsKey(Slot.WEAPON) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.WEAPON]; // Get the weapon in inventory
                    double damage = (double)(weapon.DamagePerSecond * (1 + BaseStatistics.Dexterity / 100)); // Damage formula
                    return damage;
                }

                return Dmg; // Needed for all code paths
            }
            catch (InvalidWeaponException)
            {
                // If exception throw error
                throw new InvalidWeaponException("Could not caluclate the damage with the weapon!");
            }
        }
        #endregion
    }
}
