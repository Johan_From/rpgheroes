﻿using RPGHeroes.Enums;
using RPGHeroes.Exceptions;
using RPGHeroes.HeroItems;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Heroes
{
    public class Mage : Hero
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) : base(name, AttributeCreator.MageStartingAttributes(), new Material[] { Material.CLOTH },
            new Weapons[] { Weapons.BIG_STICK, Weapons.SMALL_STICK }) {} 
        #endregion

        #region Methods
        /// <summary>
        /// Method for leveling up
        /// </summary>
        public override void LevelUp()
        {
            Level += 1; // Level + 1
            BaseStatistics += AttributeCreator.MageLevelUp(); // baseStatistics get new attributes from MageLevelUp()
        }

        /// <summary>
        /// Method for calculating the total attributes
        /// </summary>
        /// <param name="mage"></param>
        /// <exception cref="Exception"></exception>
        public void CalculateTotalAttributes(Mage mage)
        {
            TotalStatistics = BaseStatistics; // totalStatistics is basestatistics which means total != null
            foreach (var item in mage.Inventory) // Loop through the Dictionary
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor)) // If item value is Armor then do the calculation
                    {
                        Armor? armor = item.Value as Armor; // Armor is the values
                        TotalStatistics += armor!.Attributes; // Add the attributs from armor to total statistics
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Could not caluclate attributes");
                }
            }
        }

        /// <summary>
        /// Caluclates the damage
        /// </summary>
        /// <param name="mage"></param>
        /// <returns>Return damage typeof double</returns>
        public double CalculateDamage(Mage mage)
        {
            try
            {
                // If Dictionary not contains the key of enum WEAPON
                if (mage.Inventory.ContainsKey(Slot.WEAPON) == false) return Dmg;
             
                if(mage.Inventory.ContainsKey(Slot.WEAPON) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.WEAPON]; // Get the weapon in inventory
                    double damage = (double)(weapon.DamagePerSecond * (1 + BaseStatistics.Intelligence / 100)); // Damage formula
                    return damage;
                }

                return Dmg; // Needed for all code paths
            }
            catch (InvalidWeaponException)
            {
                // If exception throw error
                throw new InvalidWeaponException("Could not caluclate the damage with the weapon!");
            }
        }
        #endregion
    }
}
