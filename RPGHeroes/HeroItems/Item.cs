﻿using RPGHeroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.HeroItems
{
    public class Item : IItem
    {
        #region Varibles
        private string? _name;
        private int _requiredLevel;
        private Slot _requiredSlot;

        #endregion
        #region Properties
        // Item has a name an required level and an required slot
        public string? Name { get { return _name; } set { _name = value; } }
        public int RequiredLevel { get { return _requiredLevel; } set { _requiredLevel = value; } }
        public Slot RequiredSlot { get { return _requiredSlot; } set { _requiredSlot = value; } }

        //public string Name { get; set; }  
        //public int RequiredLevel { get; }
        //public Slot RequiredSlot { get; }
        #endregion

        #region Constuctor and methods
        /// <summary>
        /// Constructor for Item
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="requiredSlot"></param>
        public Item(string name, int requiredLevel, Slot requiredSlot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            RequiredSlot = requiredSlot;
        }

        /// <summary>
        /// Method for getting the level
        /// </summary>
        /// <returns></returns>
        public int GetLevel() => RequiredLevel;

        /// <summary>
        /// Gets the required slot
        /// </summary>
        /// <returns>Returns the requiredslot</returns>
        public Slot GetSlot() => RequiredSlot;
        #endregion
    }
}
