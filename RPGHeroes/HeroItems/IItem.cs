﻿using RPGHeroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.HeroItems
{
    /// <summary>
    /// Blueprint for item class
    /// </summary>
    internal interface IItem
    {
        public int GetLevel();
        
        public Slot GetSlot();
    }
}
