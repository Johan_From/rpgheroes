﻿using RPGHeroes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.HeroItems
{
    public class Weapon : Item
    {
        #region Variables
        private int _damage;
        private double _attackSpeed;
        private double? _damagePerSecond;

        #endregion

        #region Attributes
        // Weapon has damage attack speed dps and what kind of weapon from enum, auto prop on Weapons
        public int Damage { get { return _damage; } set { _damage = value; } }
        public double AttackSpeed { get { return _attackSpeed; } set { _attackSpeed = value; } }
        public double? DamagePerSecond { get { return _damagePerSecond; } set { _damagePerSecond = value; } }

        public Weapons Type { get; }


        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for weapon
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="damage"></param>
        /// <param name="attackSpeed"></param>
        /// <param name="type"></param>
        public Weapon(string name, int requiredLevel, int damage, double attackSpeed, Weapons type) 
            : base(name, requiredLevel, Slot.WEAPON)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
            Type = type;
            DamagePerSecond = damage * attackSpeed;
        }

        /// <summary>
        /// Method for Weapon
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>

        #endregion

        #region Methods

        /// <summary>
        /// Boolean method for cheking if equal
        /// </summary>
        /// <param name="other"></param>
        /// <returns>True or False</returns>
        public bool Equals(Weapon checker) => Name == checker.Name &&  
                                              RequiredLevel == checker.RequiredLevel && 
                                              RequiredSlot == checker.RequiredSlot && 
                                              Damage == checker.Damage &&
                                              AttackSpeed == checker.AttackSpeed && 
                                              DamagePerSecond == checker.DamagePerSecond && 
                                              Type == checker.Type;

        /// <summary>
        /// Gets the weapon type
        /// </summary>
        /// <returns>Returns weapon Type</returns>
        public Weapons GetWeaponType() => Type;
        #endregion
    }

}
