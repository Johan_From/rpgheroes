﻿using RPGHeroes.Enums;
using RPGHeroes.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.HeroItems
{
    public class Armor : Item
    {
        #region Attributes
        // Can use autoproperty here
        public Material Material { get; }
        public AttributesPowers Attributes { get; }

        #endregion

        #region Constructor
        /// <summary>
        /// Constcuctor for Armor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="requiredSlot"></param>
        /// <param name="material"></param>
        /// <param name="attributes"></param>
        public Armor(string name, int requiredLevel, Slot requiredSlot, Material material, AttributesPowers attributes) 
            : base(name, requiredLevel, requiredSlot)
        {
            Material = material;
            Attributes = attributes;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method for Equals the armor
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Armor other) =>  Name == other.Name && 
                                            RequiredLevel == other.RequiredLevel && 
                                            RequiredSlot == other.RequiredSlot && 
                                            Material == other.Material && 
                                            Attributes.Equals(other.Attributes);


        /// <summary>
        /// Get method for GetArmorMaterial
        /// </summary>
        /// <returns></returns>
        public Material GetArmorMaterial() => Material;
        #endregion
    }

}
