﻿using RPGHeroes.Main;

namespace RPGHeroes
{
    public class Program
    {
        /// <summary>
        /// Application runs from here
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("Welcome to RPG Heroes");
            Console.WriteLine("----------------------");

            // Bool for handling when program will close
            bool isRunning = true;
            while (isRunning)
            {
                // Display the Main Menu()
                isRunning = Displayer.MainMenu();
            }

            
        }
    }
}