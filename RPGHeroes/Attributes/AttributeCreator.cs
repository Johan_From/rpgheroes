﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Attributes
{
    // This class is for making it much easier for creating the attributes for making my life easier

    /// <summary>
    /// Class purpose is to easier create the attributes for the individual heroes
    /// </summary>
    public class AttributeCreator
    {
        #region Create
        /// <summary>
        /// Creates the attributes
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        /// <returns></returns>
        public static AttributesPowers CreateAttributes(int strength, int dexterity, int intelligence) => new(strength, dexterity, intelligence);

        #endregion
        
        #region Starting Attributes
        /// <summary>
        /// Warriors starting attributes
        /// </summary>
        /// <returns>Created Warrior attributes</returns>
        public static AttributesPowers WarriorStartingAttributes() => new(5, 2, 1);

        /// <summary>
        /// Mage starting attributes
        /// </summary>
        /// <returns>Created Mage attributes</returns>
        public static AttributesPowers MageStartingAttributes() =>  new(1, 1, 8);

        /// <summary>
        /// Rogue starting attributes
        /// </summary>
        /// <returns>Created Rogue attributes</returns>
        public static AttributesPowers RogueStartingAttributes() => new(2, 6, 1);
        
        /// <summary>
        /// Ranger starting attributes
        /// </summary>
        /// <returns>Created Ranger attributes</returns>
        public static AttributesPowers RangerStartingAttributes() => new(1, 7, 1);
        #endregion

        #region Level up
        /// <summary>
        /// Values for leveling up warrior
        /// </summary>
        /// <returns>Level up stats for warrior</returns>
        public static AttributesPowers WarriorLevelUp() => new(3, 2, 1);

        /// <summary>
        /// Values for leveling up Mage
        /// </summary>
        /// <returns>Level up stats for Mage</returns>
        public static AttributesPowers MageLevelUp() => new(1, 1, 5);

        /// <summary>
        /// Values for leveling up Rogue
        /// </summary>
        /// <returns>Level up stats for Rogue</returns>
        public static AttributesPowers RogueLevelUp() => new(1, 4, 1);

        /// <summary>
        /// Values for leveling up Ranger
        /// </summary>
        /// <returns>Level up stats for Ranger</returns>
        public static AttributesPowers RangerLevelUp() =>  new(1, 5, 1);

        #endregion
    }
}