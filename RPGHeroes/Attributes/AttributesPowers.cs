﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes.Attributes
{
    // Needed to have Attributes + Powers becuse compiler error of name
    public class AttributesPowers
    {
        #region Variables
        private int _strength;
        private int _dexterity;
        private int _intelligence;

        #endregion

        #region Attributes
        // Attributes has only Strenght, Desterity and Intelligence
        public int Strength { get { return _strength; } set { _strength = value; } }
        public int Dexterity { get { return _dexterity; } set { _dexterity = value; } }
        public int Intelligence { get { return _intelligence; } set { _intelligence = value; } }

        //public int Strength { get; set; }
        //public int Dexterity { get; set; }
        //public int Intelligence { get; set; }
        #endregion

        /// <summary>
        /// Constructor for attributes
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public AttributesPowers(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Bool that returns the strenght
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Strenght</returns>
        public bool Equals(AttributesPowers checker) => Strength == checker.Strength && Dexterity == checker.Dexterity && Intelligence == checker.Intelligence;

        /// <summary>
        /// Method for adding new attributes with +
        /// </summary>
        /// <param name="old"></param>
        /// <param name="newer"></param>
        /// <returns>New attributes</returns>
        public static AttributesPowers operator +(AttributesPowers? old, AttributesPowers newer) => new(old!.Strength + newer.Strength, old.Dexterity + newer.Dexterity, old.Intelligence + newer.Intelligence);
    }
}
