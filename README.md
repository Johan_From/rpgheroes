<h1>RPG Heroes</h1>
An C# console application that focuses on unit testing with XUnit. The classes are Mage, Warrior, Rogue and Ranger that can equip weapons and armor that are classes of their own. The application itself can display test objects of these classes but "run" this application properly, run the tests. This repository contains two projects, the appliciation itself RPGHeroes and the XUnit project RPGHeroesTests.

Funny note: Do not try to over complicate everything with interfaces everywhere that was my problem :/

<h2>General Information</h2>
<ul>
<li>The app written in C# (.NET 6) and utalizes XUnit for testing.</li>
<li>The app's purpose is use classes, inheritance, implementation of interfaces and much more.</li>
<li>The app's purpose is to utilize the technologies below in a testing manner.</li>

</ul><h2>Technologies Used</h2>
<ul>
<li>C# (.NET 6) </li>
</ul><ul>
<li>XUnit</li>
</ul><h2>Setup</h2>
<p>To get the console application started started follow the steps below:</p>


1. This application was written with Visual Studio 2022 
2. Clone this repository ```https://gitlab.com/Johan_From/rpgheroes.git```
3. Find ```.sln``` file and double click it
4. Right click on ```RPGHeroesTests``` and click ```Run Tests```
5. The console application and the tests are in the same project so press F5 or press the green start arrow to run the application

<h2>Collaborators</h2>
<ul>
<li><a href="https://gitlab.com/Johan_From" target="_blank">Johan From</a></li>
</ul>
